<?php
$x = 1;
$y = "10";
var_dump($x == $y);
$x = 10;
echo "<br>";
$y = "10";
var_dump($x === $y); //false because type are not equal
echo "<br>";
// !=
$x = 10;
$y = "100";
var_dump($x != $y); //return true
echo "<br>";
// <>
$x = 100;
$y = "100";
var_dump($x <> $y);
echo "<br>";
// !==
$x = 50;
$y = "50";
var_dump($x !== $y);
echo "<br>";
// >
$x = 10;
$y = 5;
var_dump($x > $y);
echo "<br>";
// <
$x = 10;
$y = 50;
var_dump($x < $y);
echo "<br>";
// >=
$x = 5;
$y = 50;
var_dump($x >= $y);
echo "<br>";
// <=
$x = 45;
$y = 50;
var_dump($x <= $y);
?>