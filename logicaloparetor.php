<?php
//and
$x = 10;
$y = 5;
if ($x == 10 and $y == 5) {
    echo "Hello bitm!";
}
echo "<br>";
//or
$x = 100;
$y = 50;

if ($x == 100 or $y == 80) {
    echo "Hello or";
}
echo "<br>";
//xor
$x = 100;
$y = 50;

if ($x == 100 xor $y == 80) {
    echo "Hello xor!"; }
    echo "<br>";
//&&
$x = 100;
$y = 50;
if ($x == 100 && $y == 50) {
    echo "Hello AND!";
}
echo "<br>";
//Or
$x = 100;
$y = 50;
if ($x == 100 || $y == 80) {
    echo "Hello Or";
}
?>